module.exports = function filter(a,fun) {
    let b =[]
    for(let idx=0;idx<a.length;idx++){
        const res = fun(a[idx],idx,a)
        if (res === true) b.push(a[idx])
    }
    return b
}