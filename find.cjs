module.exports = function find(a,fun) {
    for (let x of a){
        const res = fun (x);
            if (res) return x;
        }
    
    return undefined;
}