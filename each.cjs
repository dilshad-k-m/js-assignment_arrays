function each(a,f) {
    for (let i in a) {
        console.log(f(a,i))
    }
}
function cube(x,index) {
    if (!Array.isArray(x) || x.length == 0) {
        return []
    }
    return x[index]**3
} 
module.exports = {each,cube}