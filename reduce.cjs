module.exports = function reduce(a,fun,m) {
    let s=0
    if (m===undefined){
        m=a[0]
        s=1
    }
    for (let idx=s;idx<a.length;idx++){
        m = fun(m,a[idx],idx,a);
    }
    return m;
}