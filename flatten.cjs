module.exports = function flatten(nes,depth= 1) {
    let a=[]
    nes.forEach(element => {
        if(depth==0) {a.push(element)
        }
        else if (Array.isArray(element)) {
            a= a.concat(flatten(element,depth-1))
        }
         else {
            a.push(element)
        }
    });
    return a
}